import { Poppins, Inter } from 'next/font/google';
import localFont from 'next/font/local'

export const poppins = Poppins({ 
  subsets: ['latin'], 
  weight: ['400', '500', '600'],
}) 

export const inter = Inter({ 
  subsets: ['latin'], 
  weight: ['400', '500', '600'],
}) 

export const veneer = localFont({
  src: './Veneer/VeneerThree.otf',
})

