'use client';

import React from "react";
import clsx from "clsx";

import { LogoTitle } from "../Hero/components/LogoTitle";
import { FooterDates } from "./components/FooterDates";
import { FooterNav } from "./components/FooterNav";
import { inter } from "../../../public/fonts/fonts";
import { FooterAuthors } from "./components/FooterAuthors";

export const SectionFooter = () => {

  return (
    <div className="relative bg-[#18E6A0] w-full pb-12 lg:pt-8 lg:px-5 2xl:px-[108px]">
      <div className="absolute left-0 bottom-0 right-0 top-0 bg-texture-footer-mobile lg:bg-texture-footer bg-cover opacity-50"/>
      <div className="lg:flex items-center lg:justify-between">
        <LogoTitle 
          className="lg:!justify-start lg:pt-0 lg:!w-fit"
        />
        <FooterDates />
      </div>
      <div className="flex flex-col px-5 md:px-0 md:flex-row md:items-start md:w-[500px] md:mx-auto md:pt-14 lg:block lg:mx-0 lg:w-full z-50">
        <div className={clsx("w-full border-y border-gray-500 mt-6 pt-6 md:flex-1 md:border-0 md:mt-0 md:pt-0 xl:flex xl:justify-between xl:items-center xl:mx-0 z-50", `${(inter.className)}`)}>
          <p className="text-xs mb-6 lg:mb-3 xl:mb-0 z-50">© Sanctus Pro Deo 2023. All rights reserved.</p>
          <FooterNav />
        </div>
        <FooterAuthors />
      </div>
    </div>
  )
}