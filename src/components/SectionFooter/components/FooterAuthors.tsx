'use client';

import clsx from "clsx";
import React from "react";

import { inter } from "../../../../public/fonts/fonts";

export const FooterAuthors = () => {

  return (
    <div className={clsx("text-xs mt-6 md:pr-6 md:mt-0 md:w-fit xl:px-0 xl:pt-4 z-50", `${inter.className}`)}>
      <div className="lg:flex lg:items-center mb-6 lg:mb-1.5">
        <h2 className="mb-2 lg:mb-0 lg:mr-3">Graphic Design & UI</h2>
        <p className="opacity-50 mb-1.5 lg:mb-0">Mark Mattisson</p>
      </div>
      <div className="lg:flex lg:items-center">
        <h2 className="mb-2 lg:mb-0 lg:mr-3">Development</h2>
        <p className="opacity-50 mb-1.5 lg:mb-0 lg:mr-2">Johnathan Smith</p>
        <p className="opacity-50 mb-1.5 lg:mb-0 lg:mr-2">Michael Kloee</p>
        <p className="opacity-50">Hagen Cloo</p>
      </div>
    </div>
  )
}