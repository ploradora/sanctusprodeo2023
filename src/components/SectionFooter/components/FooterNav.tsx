'use client';

import React from "react";
import Link from "next/link";

const footerLinks = [
  {
    id: 1,
    href: "/store", 
    title: 'Store'
  },
  {
    id: 2,
    href: "/cantacusanctus", 
    title: '#Cantacusanctus'
  },
  {
    id: 3,
    href: "/despre", 
    title: 'Despre Noi'
  },
  {
    id: 4,
    href: "/doneaza", 
    title: 'Doneaza'
  },
  {
    id: 5,
    href: "/termeni", 
    title: 'Termeni si Conditii'
  },
  {
    id: 6,
    href: "/contact", 
    title: 'Contact'
  },
]

export const FooterNav = () => {

  return (
    <ul className="lg:flex items-center justify-between lg:mr-5 xl:mr-0 xl:w-full xl:max-w-[610px] xl:items-start z-50">
      {footerLinks.map(link => 
        <li key={link.id} className="mb-5 last:mb-8 xl:mb-0 xl:last:mb-0 z-50">
          <Link href={link.href} className="text-sm font-bold lg:mb-0 leading-tight z-50">
            {link.title}
          </Link> 
        </li>
      )}
    </ul>
  )
}