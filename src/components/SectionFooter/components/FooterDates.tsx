'use client';

import React from "react";
import clsx from "clsx";

import { veneer } from "../../../../public/fonts/fonts";

export const FooterDates = () => {
  return (
    <div className={clsx("flex items-center justify-between lg:justify-self-start lg:gap-x-6 mt-3 mx-auto lg:m-0 max-w-[500px] lg:max-w-none px-5 lg:p-0 tracking-tight", `${veneer.className}`)}>
      <div className="xl:flex xl:items-center z-50">
        <p className="text-xl xl:text-[40px] xl:opacity-50 xl:mr-2">02.10</p>
        <p className="text-[34px] leading-8 xl:text-[40px]">SIBIU</p>
      </div>
      <div className="xl:flex xl:items-center z-50">
        <p className="text-xl xl:text-[40px] xl:opacity-50 xl:mr-2">07.10</p>
        <p className="text-[34px] leading-8 xl:text-[40px]">TIMISOARA</p>
      </div>
      <div className="xl:flex xl:items-center z-50">
        <p className="text-xl xl:text-[40px] xl:opacity-50 xl:mr-2">09.10</p>
        <p className="text-[34px] leading-8 xl:text-[40px]">BUCURESTI</p>
      </div>
    </div>
  )
}