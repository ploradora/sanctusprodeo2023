'use client';

import React from "react";
import clsx from "clsx";

import { veneer } from "../../../../public/fonts/fonts";

interface TitleInterface {
  className?: string;
}

export const LogoTitle: React.FC<TitleInterface> = ({className}) => {
  return (
    <div className={clsx("w-full flex flex-col md:flex-row items-center justify-center pt-4", className)}>
      <span className={clsx("relative w-fit text-[40px] z-20 px-4 mb-4 md:mb-0 md:mr-4 after:absolute after:w-full after:h-1 after:bg-black after:left-0 after:top-1 md:after:top-2 before:absolute before:w-full before:h-1 before:bg-black before:left-0 before:bottom-1 md:before:bottom-2",`${veneer.className}`)}>TURNEU</span>
      <h1 className={clsx("text-[58px] font-normal leading-[4rem] md:text-[64px] z-20", `${veneer.className}`)}>SANCTUS PRO DEO</h1>
    </div>
  )
}