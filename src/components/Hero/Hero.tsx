'use client';

import React from "react";
import Image from "next/image";

import { LogoTitle } from "./components/LogoTitle";
import desktopEuSunt from '../../../public/images/desktopEuSunt.svg';
import hand1 from '../../../public/images/purpleHandMobile.png';
import hand2 from '../../../public/images/grayHandMobile.png';
import xIllustration from '../../../public/images/xIllustration.svg'
import mobileLinesHero from '../../../public/images/mobileLinesHero.svg'
import crossMobile from '../../../public/images/crossMobile.svg'
import lightningMobile from '../../../public/images/lightningMobile.svg'
import lightning from '../../../public/images/lightning.svg'
import questionMarks from '../../../public/images/questionMarks.svg'
import handsHeroDesktop from '../../../public/images/handsHeroDesktop.png'
import ESC from '../../../public/images/ESC.svg'
import linesHeroTop from '../../../public/images/linesHeroTop.svg'
import xHeroTopRight from '../../../public/images/xHeroTopRight.svg'

export const Hero = () => {
  return (
    <div className="relative w-full h-screen">
      <div className="relative w-full h-screen bg-[#18E6A0] overflow-hidden">
        <div className="grid place-items-center">
          <Image
            alt="hero svg message"
            src={desktopEuSunt}
            className="absolute top-44 sm:top-30 md:top-24 xl:top-16 w-11/12 max-w-[343px] md:max-w-[600px] md:h-[calc(100%_-_100px)] lg:h-[calc(100%_-_20px)]"
            />
          <Image 
            alt="hand purple"
            src={hand1}
            className="absolute sm:hidden -top-2 left-1/2 transform -translate-x-1/6 min-[525px]:-top-10 min-[525px]:translate-x-10 z-20"
            />
          <Image 
            alt="letter x illustration"
            src={xIllustration}
            className="absolute lg:hidden xl:block -top-2 left-3 xl:left-1/2 xl:transform xl:-translate-x-[450px] xl:top-2 z-20"
            />
          <Image 
            alt="lines illustrations"
            src={mobileLinesHero}
            className="absolute bottom-8 right-4 scale-125 z-20"
            />
          <Image 
            alt="cross illustration"
            src={crossMobile}
            className="absolute bottom-20 left-1/2 transform translate-x-16 md:translate-x-[200px] lg:left-auto lg:right-72 lg:translate-x-auto lg:bottom-0 lg:top-1/2 lg:scale-150 xl:scale-[1.8] xl:right-80 z-20"
            />
          <Image 
            alt="lightning illustration"
            src={lightningMobile}
            className="absolute md:hidden top-80 left-1/2 transform -translate-x-32 z-20"
            />
          <Image 
            alt="lightning illustration"
            src={lightning}
            className="absolute hidden md:block top-1/2 left-1/2 transform -translate-x-[43%] -translate-y-[53%] lg:-translate-y-[55%] lg:-translate-x-[46%] xl:-translate-x-[48%] xl:-translate-y-[72%] z-50"
            />
          <Image 
            alt="question marks illustration"
            src={questionMarks}
            className="absolute hidden sm:block top-40 left-4 sm:scale-75 lg:scale-100 lg:top-1/2 transform lg:-translate-y-[55%] z-20"
            />
          <Image 
            alt="Esc key illustration"
            src={ESC}
            className="absolute hidden lg:block bottom-1 left-1/2 transform -translate-x-1/2 sm:scale-75 z-20"
            />
          <Image 
            alt="x letter illustration"
            src={xHeroTopRight}
            className="absolute hidden lg:block lg:top-4 lg:right-10 xl:right-10 xl:top-20 sm:scale-75 z-20"
            />
          <Image 
            alt="lines illustration"
            src={linesHeroTop}
            className="absolute hidden lg:block -top-12 left-0 z-20"
            />
        </div>
        <LogoTitle />
      </div>
      <Image 
        alt="hand gray"
        src={hand2}
        className="absolute sm:hidden top-[500px] left-1/2 transform -translate-x-80 min-[525px]:-translate-x-96 min-[525px]:top-[550px] z-20"
      />
      <Image 
        alt="hands"
        src={handsHeroDesktop}
        className="absolute hidden sm:block top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 scale-110 xl:-translate-y-[58%] z-20"
      />
    </div>
  )
}