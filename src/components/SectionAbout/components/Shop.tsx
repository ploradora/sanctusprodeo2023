'use client';

import React from "react";
import clsx from "clsx";

import { poppins, inter } from "../../../../public/fonts/fonts";

export const Shop = () => {
  return (
    <div className="xl:px-5 2xl:px-[108px] z-50">
      <div className="md:flex items-center lg:px-5 lg:pb-8 lg:pt-5 xl:px-0">
        <div className="relative h-[400px] sm:h-[500px] w-full mb-2 md:mb-0 lg:mr-2 bg-[url('/images/tshirts.png')] bg-cover bg-center">
          <div className="w-full h-full bg-neutral-900 opacity-50"/>
          <p className={clsx("absolute w-full bottom-12 text-center text-white font-bold text-2xl tracking-tight z-40", `${poppins.className}`)}>T-Shirt (UNISEX)</p>
          <p className={clsx("absolute w-full bottom-5 text-center text-white opacity-50 z-20", `${inter.className}`)}>XS/S/M/L</p>
        </div>
        <div className="relative h-[400px] sm:h-[500px] w-full bg-[url('/images/wristband.png')] bg-cover bg-center">
          <div className="w-full h-full bg-neutral-900 opacity-50"/>
          <p className={clsx("absolute w-full bottom-12 text-center text-white font-bold text-2xl tracking-tight z-40", `${poppins.className}`)}>BRATARA DE PIELE</p>
          <p className={clsx("absolute w-full bottom-5 text-center text-white opacity-50 z-20", `${inter.className}`)}>One-Size</p>
        </div>
      </div>
      <div className={clsx("flex items-top text-white px-6 pt-10 pb-14 lg:px-4 lg:pt-4 lg:mx-5 xl:mx-0 lg:border-t lg:border-white opacity-50", `${inter.className}`)}>
        <p className="opacity-50 mr-2">*</p>
        <p className="opacity-50">Tricourile și brățările sunt disponibile doar fizic, în locațiile concertelor.</p>
      </div>
    </div>
  )
}