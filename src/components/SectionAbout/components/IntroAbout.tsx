'use client';

import React from "react";
import clsx from "clsx";
import Link from "next/link";
import Image from "next/image";

import { inter, poppins } from "../../../../public/fonts/fonts";
import linesAbout from '../../../../public/images/linesAbout.svg';
import doodleAbout from '../../../../public/images/doodleAbout.svg';
import hands from '../../../../public/images/hands.png';
import handsAboutLarge from '../../../../public/images/handsAboutLarge.png';
import x1 from '../../../../public/images/x1About.svg';
import x2 from '../../../../public/images/x2About.svg';

export const IntroAbout = () => {
  
  return (
    <div className="pb-6 xl:px-5 xl:pr-0 2xl:pl-[108px] xl:w-1/2 xl:pt-10 2xl:pt-10 xl:flex xl:items-center xl:justify-between xl:flex-row-reverse xl:w-full">
      <Image 
          alt="hands"
          src={handsAboutLarge}
          className="absolute hidden xl:inline-block left-1/2 transform -translate-x-1/2 xl:scale-[1.13] z-10 select-none"
        /> 
      <div className="relative w-full h-[500px] xl:w-1/2 xl:h-[700px] overflow-hidden">
        <Image 
          alt="doodle illustration"
          src={doodleAbout}
          className="absolute top-6 left-[20px] min-[550px]:left-[50px] md:left-[100px] xl:top-1"
        /> 
        <Image 
          alt="x bottom illustration"
          src={x1}
          className="absolute top-32 left-1/2 transform -translate-x-[100px] min-[550px] md:-translate-x-[150px]"
        /> 
        <Image 
          alt="x top illustration"
          src={x2}
          className="absolute top-14 left-1/2 transform -translate-x-2/3 sm:translate-x-[80px] xl:top-1"
        /> 
        <Image 
          alt="hands"
          src={hands}
          className="absolute top-1/2 left-1/2 transform -translate-y-2/3 -translate-x-1/2 scale-[1.6] min-[515px]:rotate-[5deg] sm:rotate-[20deg] sm:scale-[1.4] sm:-translate-y-[57%] min-[939px]:scale-[1.65] min-[1110px]:hidden z-20 select-none"
        /> 
        <Image 
          alt="hands"
          src={handsAboutLarge}
          className="absolute hidden min-[1110px]:block xl:hidden top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-[55%] rotate-[5deg] z-20 select-none"
        /> 
        <Image 
          alt="lines illustrations"
          src={linesAbout}
          className="absolute top-1/2 left-1/2 transform -translate-x-1/2 sm:-translate-x-1/3  sm:scale-110 md:right-10 md:-translate-x-[10%] min-[939px]:translate-x-10 min-[939px]:scale-[2] xl:scale-150"
        /> 
      </div>
      <div className="xl:w-1/2 xl:pb-[250px]">
        <p className={clsx("text-white text-center text-start font-normal leading-6 px-5 pt-5 pb-14 lg:pt-8 xl:pb-8 xl:pl-0", `${inter.className}`)}>
          În contextul agitat pe care-l traversăm, sub presiunea responsabilităților și evenimentelor, pare că tot ce mai reușim să facem este doar să reacționăm la ce se întâmplă sau să ne exprimăm câte o părere sau alegere. Furați de peisajul în schimbare, uităm ușor că în asemenea momente e necesar să ne întoarcem la valorile și întrebările de bază ale existenței, ca să ne asigurăm că suntem pe drumul corespunzător. Cine sunt? Încotro merg? Care e scopul existenței mele?
        </p>
        <p className={clsx("text-[#18E6A0] text-[28px] text-center text-start font-bold leading-7 uppercase px-5 pb-14 xl:pb-8 xl:pl-0", `${poppins.className}`)}>
          Iar în mijlocul acestor căutări, de ce să vorbim despre Dumnezeu? De fapt, dincolo de religii, ritualuri și dogme - Cine este Dumnezeu?
        </p>
        <p className={clsx("text-white text-center text-start font-normal leading-6 px-5 pb-14 xl:pb-8 xl:pl-0", `${inter.className}`)}>
          Dacă asemenea întrebări au sens și pentru tine, te invităm să descoperim perspective valoroase împreună cu Sanctus Pro Deo la concertul “Eu sunt”.
        </p>
        <div className="w-full flex justify-center xl:justify-start">
          <Link href="/despre" className="bg-[#18E6A0] hover:bg-[#16CC8E] duration-150 w-fit py-4 px-8 font-bold rounded-full tracking-tight z-50 whitespace-nowrap">
              DESPRE SANCTUS PRO DEO
          </Link>
        </div>
      </div>
    </div>
  )
}