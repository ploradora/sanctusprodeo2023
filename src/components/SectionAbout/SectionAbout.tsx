'use client';

import clsx from "clsx";
import Link from "next/link";
import React from "react";

import { inter, poppins } from "../../../public/fonts/fonts";
import { IntroAbout } from "./components/IntroAbout";
import { Shop } from "./components/Shop";

export const SectionAbout = () => {

  return (
    <div className="bg-black">
      <IntroAbout />
      <Shop />
    </div>
  )
}