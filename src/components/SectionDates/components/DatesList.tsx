"use client";

import React from "react";
import { dates } from "./Dates";

import { DateItemWrapper } from "./DateItem/DateItemWrapper";

export const DatesList = () => {

  return (
    <div className="xl:p-5 2xl:px-[108px]">
      {dates.map((date) => (
        <DateItemWrapper 
          key={date.id} 
          cladire={date.cladire}
          data={date.data}
          ora={date.ora}
          oras={date.oras}
          strada={date.strada}
          isSoldOut={date.isSoldOut}
        />
      ))}
    </div>
  );
};
