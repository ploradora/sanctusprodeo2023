export interface ItemWrapperInterface {
  data: string;
  oras: string;
  cladire: string;
  strada: string;
  ora: string;
  isSoldOut: boolean;
}
export interface DateItemInterface {
  classNameSoldOut?: string;
  classNameButton?: string;
  data: string;
  oras: string;
  cladire: string;
  strada: string;
  ora: string;
  isSoldOut: boolean;
  textButton: string;
  handleClick: () => void;
}