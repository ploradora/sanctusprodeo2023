"use client";

import React from "react";
import clsx from "clsx";

import { poppins, veneer } from "../../../../../public/fonts/fonts";
import { DateItemInterface } from "./DateItem.types";

export const DateItem: React.FC<DateItemInterface> = ({cladire, isSoldOut, data, ora, oras, strada, classNameButton, classNameSoldOut, textButton, handleClick}) => {

  return (
    <div
      className={clsx(
        "md:flex md:items-center md:justify-between pt-4 pb-8 border-b border-black lg:border-none",
        `${veneer.className}`
      )}
    >
      <div className="lg:flex lg:items-center">
        <div className={clsx("flex items-center py-2 md:py-0 lg:w-[370px] xl:w-[420px]", classNameSoldOut)}>
          <p className={clsx("md:w-28 xl:w-40 text-[#18E6A0] text-[48px] md:text-[64px] mr-2 z-50", classNameSoldOut)}>
            {data}
          </p>
          <p className="text-[48px] md:text-[64px] z-50">{oras}</p>
        </div>
        <div className={clsx("py-2 tracking-tight z-50 lg:w-[300px] xl:w-[400px]  lg:mr-6", classNameSoldOut, `${poppins.className}`)}>
          <p className="font-bold xl:leading-6 xl:text-[22px] z-50">{cladire}</p>
          <p className="z-50">{strada}</p>
        </div>
      </div>
      <div className="flex items-center md:self-start justify-between md:w-[281px] lg:w-[270px]">
        <p className={clsx("text-[48px] md:text-[64px] mr-6 lg:mr-0 z-50", classNameSoldOut)}>{ora}</p>
        <button 
          onClick={handleClick}
          disabled={isSoldOut}
          className={clsx(
            "py-4 px-8 border-2 font-bold rounded-full tracking-tight z-50 whitespace-nowrap hover:drop-shadow-md duration-150", classNameButton, `${poppins.className}`
          )}
        >
          {textButton}
        </button>
      </div>
    </div>
  );
};
