'use client';

import React from "react";
import { DateItem } from "./DateItem";

import { ItemWrapperInterface } from "./DateItem.types";

export const DateItemWrapper: React.FC<ItemWrapperInterface> = ({cladire, data, isSoldOut, ora, oras, strada}) => {
  const classNameSoldOut = isSoldOut ? 'text-gray-400' : '';
  const classNameButton = isSoldOut ? 'bg-white border-gray-300 userselect-none' : 'bg-[#18E6A0] border-[#18E6A0]';
  const textButton = isSoldOut ? 'SOLD OUT' : 'REZERVA';

  const handleClick = () => {
    console.log('reserva')
  }
  return (
    <DateItem  
      cladire={cladire}
      data={data}
      ora={ora}
      oras={oras}
      strada={strada}
      isSoldOut={isSoldOut}
      classNameButton={classNameButton}
      classNameSoldOut={classNameSoldOut}
      textButton={textButton}
      handleClick={handleClick}
    />
    )
}