'use client';

import clsx from "clsx";
import React from "react";
import { inter, poppins } from "../../../public/fonts/fonts";
import { DatesList } from "./components/DatesList";

export const SectionDates = () => {
  return (
    <div className="mx-auto px-5 xl:px-0">
      <div className={clsx("hidden lg:flex items-center justify-between pb-2 pt-8 2xl:mx-[88px] border-b border-black font-bold text-base tracking-tight", `${poppins.className}`)}>
        <div className="flex items-center">
          <p className="w-[120px] xl:w-[190px] xl:pl-5">DATA</p>
          <p className="w-[250px]">ORAS</p>
          <p>LOCATIE</p>
        </div>
        <p className="w-[274px] xl:w-[293px]">ORA</p>
      </div>
      <DatesList />
      <p className={clsx("text-sm py-2 xl:pl-5 mb-8 2xl:pl-[108px]", `${inter.className}`)}>*Locurile sunt limitate, rezervă-ți biletul din timp înainte de SOLD OUT.</p>
    </div>
  )
}