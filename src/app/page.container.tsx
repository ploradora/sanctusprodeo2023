'use client';

import React from "react";

import { Hero } from "@/components/Hero/Hero";
import { SectionDates } from "@/components/SectionDates/SectionDates";
import { SectionAbout } from "@/components/SectionAbout/SectionAbout";
import { SectionVideo } from "@/components/SectionVideo/SectionVideo";
import { SectionFooter } from "@/components/SectionFooter/SectionFooter";

export const LandingPage = () => {

    return (
      <div className="relative overflow-hidden">
        <div className="relative">
          <div className="absolute left-0 bottom-0 right-0 top-0 bg-texture bg-cover z-10 opacity-50"/>
          <Hero />
          <SectionDates />
        </div>
        <SectionAbout />
        <SectionVideo />
        <SectionFooter />
      </div>
    )
}