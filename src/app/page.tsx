'use client';

import { LandingPage } from './page.container'

export default function Home() {

  return (
    <LandingPage />
  )
}
