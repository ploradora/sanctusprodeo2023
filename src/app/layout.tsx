import './globals.css'
import type { Metadata } from 'next'
import { poppins } from '../../public/fonts/fonts'

export const metadata: Metadata = {
  title: 'Sanctus Pro Deo',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={poppins.className}>
        {children}
      </body>
    </html>
  )
}
